<?php
    $servername = "localhost";
    $username = "root";
    $password = "";

    $conn = new mysqli($servername, $username, $password);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Create database
    $sql = "CREATE DATABASE IF NOT EXISTS testFlip";
    if ($conn->query($sql) === TRUE) {
        $createDb = "Database created successfully";
    } else {
        $createDb = $conn->error;
    }

    if ($createDb == "Database created successfully") {
        $sql = "CREATE TABLE IF NOT EXISTS transaction_user (
            id INT(11) PRIMARY KEY,
            amount INT(11),
            status VARCHAR(30),
            timestamp DATETIME,
            bank_code VARCHAR(50), 
            account_number VARCHAR(30),
            beneficiary_name VARCHAR(50),
            remark VARCHAR(30)
            receipt VARCHAR(50),
            time_served DATETIME,
            fee INT(11)
        )";
        
        if ($conn->query($sql) === TRUE) {
            echo "Table transaction_user created successfully";
        } else {
            echo "Error creating table: " . $conn->error;
        }
    }

    $conn->close();
?>