<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    require_once('../connect.php');
    require_once("../api.php");

    $id = isset($_GET['id']) ? $_GET['id'] : die();

    $sql = 'SELECT id FROM transaction_user WHERE `id` = '.$id;
    if ($conn->query($sql) === TRUE) {
        $get_data = callAPI('GET', 'https://nextar.flip.id/disburse/'.$id, false);
        $response = json_decode($get_data, true);
        return $response;
    } else {
        echo "Error : " . $conn->error;
    }

?>