<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    require_once('../connect.php');
    require_once("../api.php");

    $post = json_decode(file_get_contents("php://input"));

    if(
        !empty($post->bank_code) &&
        !empty($post->account_number) &&
        !empty($post->amount) &&
        !empty($post->remark)
    ){
        $data_array =  array(
            "bank_code" => $post->bank_code,
            "account_number" => $post->account_number,
            "amount" => $post->amount,
            "remark" => $post->remark
        );
        $make_call = callAPI('POST', 'https://nextar.flip.id', json_encode($data_array));
        $response = json_decode($make_call, true);
        if ($response) {
            $sql = 'UPDATE transaction_user SET 
                status = "'.$response['status'].'", 
                receipt = "'.$response['receipt'].'",
                time_served = "'.$response['time_served'].'"
                WHERE `id` = '.$response['id'];
            if ($conn->query($sql) === TRUE) {
                echo "Updated successfully";
            } else {
                echo "Error : " . $conn->error;
            }
        }
    }

?>